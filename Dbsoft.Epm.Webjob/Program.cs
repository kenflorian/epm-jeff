﻿namespace Dbsoft.Epm.Webjob
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Autofac;
    using AutoMapper;
    using Infrastructure;
    using DbSoft.Cache.Aspect;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.Commands;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.Market;
    using DBSoft.EVEAPI.Crest;
    using DBSoft.EVEAPI.Crest.Facility;
    using DBSoft.EVEAPI.Crest.SolarSystem;
    using NLog;
    using NLog.Targets;
    using Item = DBSoft.EPM.DAL.CodeFirst.Models.Item;
    using MarketPriceService = DBSoft.EVEAPI.Entities.MarketPrice.MarketPriceService;
    using SolarSystem = DBSoft.EPM.DAL.CodeFirst.Models.SolarSystem;

    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        static void Main()
        {
            Task.Run(() => DoMain()).Wait();
        }

        private static async Task DoMain()
        {
            ConfigureMappings();
            CacheProvider.Cache = new CacheProvider();
            CacheService.Cache = CacheProvider.Cache;
            var container = ContainerConfiguration.RegisterIocContainer();

            while (true)
            {
                UpdateSolarSystems(container);
                UpdateStations(container);
                await UpdateMarketPrices(container);
                await UpdateMarketResearch(container);
                await Task.Delay(TimeSpan.FromDays(1));
            }
            // ReSharper disable once FunctionNeverReturns
        }

        public static IConfiguration Configuration { get; set; }

        private static async Task UpdateMarketPrices(IComponentContext container)
        {
            var factory = container.Resolve<IDbContextFactory>();
            using (var context = factory.CreateContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                var items = context.Set<Item>().ToList();
                var prices = (await new MarketPriceService()
                    .GetMarketPrices())
                    .Where(p => items.Any(f => f.ID == p.ItemId));
                foreach (var price in prices)
                {
                    var p = context.Get<MarketPrice>(f => f.ItemID == price.ItemId);
                    p.ItemID = price.ItemId;
                    p.AdjustedPrice = price.AdjustedPrice;
                }
                context.ChangeTracker.DetectChanges();
                await context.SaveChangesAsync();
            }
        }

        private static void ConfigureMappings()
        {
            Mapper.CreateMap<Station, StationDTO>()
                .ForMember(m => m.StationID, opt => opt.MapFrom(m => m.ID));
            Mapper.CreateMap<SolarSystem, SolarSystemDTO>()
                .ForMember(m => m.SolarSystemID, opt => opt.MapFrom(m => m.ID))
                ;
            Mapper.CreateMap<User, UserDTO>()
                .ForMember(m => m.UserID, map => map.MapFrom(m => m.ID));

        }

        private static void UpdateStations(IComponentContext container)
        {
            var facilities = container.Resolve<IFacilityService>().GetFacilities();
            var factory = container.Resolve<IDbContextFactory>();
            foreach (var facility in facilities)
            {
                using (var context = factory.CreateContext())
                {
                    try
                    {
                        var stn = context.Set<Station>().Find(facility.FacilityId);
                        if (stn == null)
                        {
                            stn = new Station
                            {
                                ID = facility.FacilityId,
                                Name = facility.FacilityName,
                                SolarSystemID = facility.SolarSystemId,
                                Tax = facility.Tax
                            };
                            context.Set<Station>().Add(stn);
                        }
                        else
                        {
                            stn.Tax = facility.Tax;
                            stn.Name = facility.FacilityName;
                        }
                        context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Logger.Warn(e, "Saving station");
                    }
                }
            }
        }

        private static void UpdateSolarSystems(IComponentContext container)
        {
            var systems = container.Resolve<ISolarSystemService>().GetSolarSystems();
            var factory = container.Resolve<IDbContextFactory>();
            foreach (var system in systems)
            {
                using (var context = factory.CreateContext())
                {
                    var sys = context.Set<SolarSystem>().Find(system.Id);
                    if (sys != null)
                    {
                        sys.ManufacturingCost = system.ManufacturingCost.GetValueOrDefault();
                    }
                    context.SaveChanges();
                }
            }
        }

        private static async Task UpdateMarketResearch(IComponentContext container)
        {
            var hubs = container.Resolve<IConfigurationService>().ListMarketSellLocations();
            var service = container.Resolve<IMarketResearchService>();
            var config = container.Resolve<IEpmConfig>();
            var auth = container.Resolve<IUserAuth>();
            var users = container.Resolve<IUserService>();

            var refresh = users.List().Single(f => f.UserID == 1).SsoRefreshToken;

            if (refresh != null)
            {
                var clientId = config.GetSetting("Authentication:EveSso:ClientId");
                var secret = config.GetSetting("Authentication:EveSso:ClientSecret");
                foreach (var hub in hubs)
                {
                    var access = (await auth.RefreshAuthenticatedUser(refresh, clientId, secret)).Token;
                    service.Update(access, hub);
                    Logger.Info("Updated hub {0}", hub);
                }
            }
        }

        private static void ConfigureLogTargets(IComponentContext container)
        {
            var config = LogManager.Configuration;
            var target = (DatabaseTarget)config.FindTargetByName("database");
            target.ConnectionString = new ConnectionStringBuilder(container.Resolve<IEpmConfig>()).Build();
            LogManager.ReconfigExistingLoggers();
        }
    }
}
