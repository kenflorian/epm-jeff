﻿namespace EPM.Test.Unit.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.Market;
    using DBSoft.EPM.Logic.Config;
    using DBSoft.EPM.Logic.RefreshApi;
    using DBSoft.EVEAPI.Crest;
    using DBSoft.EVEAPI.Crest.MarketOrder;
    using DBSoft.EVEAPI.Entities.MarketOrder;
    using Microsoft.Framework.OptionsModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;
    using MarketOrderDTO = DBSoft.EVEAPI.Crest.MarketOrder.MarketOrderDTO;
    using MarketOrderRequest = DBSoft.EVEAPI.Crest.MarketOrder.MarketOrderRequest;

    [TestClass]
    public class RefreshApiTests
    {
        [TestMethod]
        public void MarketImportMapper_ImportsMaterialSellPrices()
        {
            var items = Substitute.For<IItemService>();
            items.ListBuildable(Arg.Any<string>()).Returns(new List<BuildableItemDTO>());

            var config = Substitute.For<IConfigurationService>();
            var universe = Substitute.For<IUniverseService>();
            universe.GetStationSolarSystem(Arg.Any<int>()).Returns(new SolarSystemDTO());
            var users = Substitute.For<IUserService>();
            users.GetAuthenticatedUser(Arg.Any<string>()).Returns(new UserDTO());
            var options = Substitute.For<IOptions<Authentication>>();
            options.Options.Returns(new Authentication {EveSso = new EveSso()});
            var auth = Substitute.For<IUserAuth>();
            auth.RefreshAuthenticatedUser(null, null, null)
                .ReturnsForAnyArgs(Task.Run(() => new AuthenticatedUserDTO()));
            var market = Substitute.For<IMarketService>();
            var buy = new List<MarketOrderDTO> {new MarketOrderDTO {OrderType = OrderType.Buy} };
            market.ListOrders(Arg.Is<MarketOrderRequest>(f => f.OrderType == OrderType.Buy))
                .Returns(buy);
            var sell = new List<MarketOrderDTO> { new MarketOrderDTO { OrderType = OrderType.Sell } };
            market.ListOrders(Arg.Is<MarketOrderRequest>(f => f.OrderType == OrderType.Sell))
                .Returns(sell);
            var materials = Substitute.For<IMaterialItemService>();
            materials.List(Arg.Any<string>()).Returns(new List<MaterialItemDTO>());
            var importer = Substitute.For<IMarketImportService>();
            var status = Substitute.For<IEveApiStatusService>();
            var jumps = Substitute.For<IJumpService>();
            var mapper = new MarketImportMapper(market, materials, items, config, universe, importer, status, jumps, options, auth, users);
            mapper.Pull(null).Wait();
            importer.Received()
                .SaveMarketImports(Arg.Any<DateTime>(),
                    Arg.Is<List<SaveMarketImportRequest>>(
                        f =>
                            f.Any(any => any.OrderType == DBSoft.EPM.DAL.CodeFirst.Models.OrderType.Buy) &&
                            f.Any(any => any.OrderType == DBSoft.EPM.DAL.CodeFirst.Models.OrderType.Sell)));
        }
    }
}
