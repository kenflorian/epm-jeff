﻿namespace EPM.Test.Unit
{
    using System.Collections.Generic;
    using System.Linq;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    [TestClass]
    public class AssetValueServiceTests
    {
        [TestMethod]
        public void WhenMultiRegions_AssetsByStation_ShouldShowBothRegions()
        {
            var config = new UnitTestConfig().MultiRegion();

            var assets = Substitute.For<IAssetService>();
            assets.ListByItemAndStation(Arg.Any<AssetServiceRequest>()).Returns(f => new List<AssetByItemAndStationDTO>
            {
                // The whole point is this service gets called not once but twice
                new AssetByItemAndStationDTO
                {
                    ItemID = 1,
                    Quantity = 1
                }
            });
            var prices = Substitute.For<IMarketPriceService>();
            prices.List(Arg.Any<MarketPriceRequest>()).Returns(f => new List<MarketPriceDTO>
            {
                new MarketPriceDTO
                { 
                    ItemID = 1,
                    CurrentPrice = 1
                }
            });
            var service = new AssetValueService(config.Config, assets, prices, config.Universe, null);

            var request = new AssetValueRequest
            {
                Token = "token"
            };
            var result = service.ListByItemAndStation(request);
            Assert.AreEqual(2, result.Sum(f => f.Value));
        }
    }
}
