﻿namespace EPM.Test.Unit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Enums;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.ItemCosts;
    using DBSoft.EPM.DAL.Services.Market;
    using DBSoft.EPM.DAL.Services.MaterialCosts;
    using DBSoft.EPM.DAL.Services.Transactions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    [TestClass]
    public class FreightCostTests
    {
        private readonly IConfigurationService _config;
        private readonly IMarketPriceService _prices;
        private readonly EpmEntitiesFactory _factory;
        private readonly IItemTransactionService _purchases;
        private readonly IUserService _users;
        private readonly MaterialCostRequest _request;
        private readonly IMaterialCostService _costs;
        private readonly IItemService _items;
        private readonly IBuildMaterialService _materials;
        private readonly IUniverseService _universe;
        private readonly FreightCalculator _calculator;

        private const int Tritanium = 34;
        private const int Zydrine = 39;
        const int Raven = 638;
        const int Missing = 99999;

        public FreightCostTests()
        {
            var config = new EpmConfig();
            var provider = new ConnectionStringProvider(config);
            _factory = new EpmEntitiesFactory(provider);

            _config = Substitute.For<IConfigurationService>();
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightPickupCost).Returns(3500000M);
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightJumpCost).Returns(1500000M);
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightMaxVolume).Returns(845000);
            _config.GetSetting<decimal>(Arg.Any<string>(), ConfigurationType.FreightMaxCollateral).Returns(1000000000M);

            var jumps = Substitute.For<IJumpService>();
            jumps.GetJumps(0, 0).ReturnsForAnyArgs(f => (short)3);

            var materials = Substitute.For<IMaterialItemService>();
            var producible = new List<MaterialItemDTO> { new MaterialItemDTO { Volume = .01M } };
            materials.List(null).ReturnsForAnyArgs(f => producible);

            _materials = Substitute.For<IBuildMaterialService>();
            _materials.ListBuildable(Arg.Any<string>()).Returns(new List<BuildMaterialDTO> 
            { 
                new BuildMaterialDTO { ItemID = Raven, Quantity = 1, MaterialID = Tritanium },
                new BuildMaterialDTO { ItemID = Missing, Quantity = 1, MaterialID = Tritanium } 
            });

            _prices = Substitute.For<IMarketPriceService>();
            _prices.List(null).ReturnsForAnyArgs(new List<MarketPriceDTO> 
            { 
                new MarketPriceDTO { ItemID = Tritanium, CurrentPrice = 5 },
                new MarketPriceDTO{ItemID = Zydrine, CurrentPrice = 1500}
            });

            _purchases = Substitute.For<IItemTransactionService>();
            _purchases.ListByItem(Arg.Any<ItemTransactionRequest>()).Returns(new List<ItemTransactionByItemDTO>());

            _users = Substitute.For<IUserService>();
            _users.GetUserID(Arg.Any<string>()).Returns(1);

            _items = Substitute.For<IItemService>();
            _items.ListBuildable(Arg.Any<string>()).Returns(
                new List<BuildableItemDTO> 
                { 
                    new BuildableItemDTO 
                    { 
                        ItemID = Raven, 
                        QuantityMultiplier = 1, 
                        Volume = 50000 
                    }, 
                    new BuildableItemDTO
                    {
                        ItemID = Missing,
                        QuantityMultiplier = 1,
                        Volume = 50000
                    }
                });

            _costs = Substitute.For<IMaterialCostService>();
            _costs.List(Arg.Any<MaterialCostRequest>()).Returns(new List<MaterialCostDTO> { new MaterialCostDTO { MaterialID = Tritanium } });

            _universe = Substitute.For<IUniverseService>();
            _universe.GetStation(Arg.Any<int>()).Returns(new StationDTO());
            _universe.ListSolarSystems().Returns(new List<SolarSystemDTO> { new SolarSystemDTO() });
            _universe.GetAdjustedMarketPrices().Returns(new List<MarketAdjustedPriceDTO> 
            { 
                new MarketAdjustedPriceDTO { ItemID = Missing, AdjustedPrice = 200000000 }, 
                new MarketAdjustedPriceDTO { ItemID = Raven, AdjustedPrice = 200000000 },
                new MarketAdjustedPriceDTO { ItemID = Tritanium, AdjustedPrice = 5 }
            });
            _universe.GetStationSolarSystem(Arg.Any<int>()).Returns(new SolarSystemDTO());

            _request = new MaterialCostRequest();
            _calculator = new FreightCalculator(_config, jumps, _universe);

            using (var context = _factory.CreateContext())
            {
                var extension = context.Set<ItemExtension>().Single(f => f.UserID == 1 && f.ItemID == Raven);
                extension.MinimumStock = 8;
                context.SaveChanges();
            }
        }

        [TestMethod]
        public void WhenMaterialFreightCostIsVolumeConstrained_MaterialCostsShouldReflectVolumeFreightCosts()
        {
            // Trit: Volume .01, price 5, jumps 3 @1.5mil, 3.5mil pickup, 845,000 capacity, 1bil max collateral: 
            var service = new MaterialCostService(_factory, _purchases, _prices, _users, _calculator);
            var result = service.List(_request);
            Assert.AreEqual(5.094M, result.First(f => f.MaterialID == Tritanium).Cost.SignificantFigures(4));
        }

        [TestMethod]
        public void WhenFreightCostIsCollateralConstrained_MaterialCostsShouldReflectFreightValueCosts()
        {
            // Zyd: Volume .01, price 1500, jumps 3 @1.5mil, 3.5mil pickup, 845,000 capacity, 1bil max collateral: 
            var service = new MaterialCostService(_factory, _purchases, _prices, _users, _calculator);
            var result = service.List(_request);
            Assert.AreEqual(1512M, result.First(f => f.MaterialID == Zydrine).Cost.SignificantFigures(4));
        }

        [TestMethod]
        public void ItemCostsShouldReflectFreightValueCosts()
        {
            _prices.List(null).ReturnsForAnyArgs(new List<MarketPriceDTO> { new MarketPriceDTO { ItemID = Raven, CurrentPrice = 200000000 } });

            var request = new ListBuildableRequest();
            var service = new ItemCostService(_costs, _items, _config, _materials, _universe, _prices, _calculator);
            var result = service.ListBuildable(request);

            Assert.AreEqual(1600000, result.First(f => f.ItemID == Raven).Cost);
        }

        [TestMethod]
        public void GivenNotConfigured_CalculatorShouldReturnZero()
        {
            var config = Substitute.For<IConfigurationService>();
            var jumps = Substitute.For<IJumpService>();
            var universe = Substitute.For<IUniverseService>();
            universe.GetStationSolarSystem(Arg.Any<int>()).Returns(new SolarSystemDTO());
            var calculator = new FreightCalculator(config, jumps, universe);
            var result = calculator.GetFreightCost(null, 1, 1);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void WhenMarketPriceNotAvailable_CalculatorShouldUseAdjustedPrice()
        {
            var configService = Substitute.For<IConfigurationService>();

            var service = new ItemCostService(_costs, _items, configService, _materials, _universe, _prices, _calculator);
            var request = new ListBuildableRequest { Token = Guid.NewGuid().ToString() };

            var result = service.ListBuildable(request);
            Assert.IsTrue(result.Single(f => f.ItemID == Missing).FreightCost == 1600000);
        }
    }
}
