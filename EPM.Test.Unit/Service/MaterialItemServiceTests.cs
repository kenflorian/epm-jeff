﻿namespace EPM.Test.Unit.Service
{
    using System.Linq;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.DAL.Services;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    [TestClass]
    public class MaterialItemServiceTests
    {
        [TestMethod]
        public void List_ShouldIncludeVolumes()
        {
            var config = new EpmConfig();
            var provider = new ConnectionStringProvider(config);
            var factory = new EpmEntitiesFactory(provider);
            var users = Substitute.For<IUserService>();
            users.GetUserID(null).ReturnsForAnyArgs(1);
            var service = new MaterialItemService(factory, users);
            var result = service.List(null);
            Assert.IsTrue(result.First().Volume != 0);
        }

    }
}
