﻿namespace EPM.Test.Unit
{
    using System.Collections.Generic;
    using System.Linq;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.Transactions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NSubstitute;

    [TestClass]
    public class MaterialPurchaseServiceTests
    {
        private readonly IAssetService _assets;
        private readonly MaterialPurchaseService _service;

        private const int Raven = 638;
        private const int MinmatarFuelBlock = 4246;
        private const int Tritanium = 34;
        private const int NitrogenIsotopes = 17888;

        public MaterialPurchaseServiceTests()
        {
            _assets = Substitute.For<IAssetService>();
            var transactions = Substitute.For<IItemTransactionService>();
            transactions.ListByItem(Arg.Any<ItemTransactionRequest>()).Returns(f => new List<ItemTransactionByItemDTO>());
            var prices = CreatePrices();
            var config = new UnitTestConfig().MultiRegion();
            var orders = Substitute.For<IMarketOrderService>();
            var materials = Substitute.For<IMaterialRequirementService>();
            CreateMaterials(materials);
            var buildable = Substitute.For<IItemService>();
            CreateBuildable(buildable);
            var productionMaterials = Substitute.For<IProductionMaterialService>();
            _service = new MaterialPurchaseService(_assets, transactions, prices, config.Config, orders, materials, buildable, 
                productionMaterials, config.Universe);
        }

        [TestMethod]
        public void WhenMultiRegion_List_ShouldReturnMultiRegionResult()
        {
            // Put one trit in each region

            _assets.ListByItem(Arg.Any<AssetServiceRequest>()).Returns(f => new List<AssetByItemDTO>
            {
                new AssetByItemDTO
                {
                    ItemID = Tritanium,
                    Quantity = 1
                }
            });

            var request = new MaterialPurchaseRequest
            {
                Token = "token"
            };
            var result = _service.List(request).ToList();
            // Both trit found?
            Assert.AreEqual(2, result.Single(f => f.ItemID == Tritanium).InventoryQuantity);
        }

        private static void CreateBuildable(IItemService buildable)
        {
            buildable.ListBuildable(Arg.Any<string>()).Returns(f => new List<BuildableItemDTO>
            {
                new BuildableItemDTO
                {
                    ItemID = Raven,
                    QuantityMultiplier = 1,
                    MinimumStock = 1
                },
                new BuildableItemDTO
                {
                    ItemID = MinmatarFuelBlock,
                    QuantityMultiplier = 40,
                    MinimumStock = 10
                }
            });
        }

        private static void CreateMaterials(IMaterialRequirementService materials)
        {
            materials.ListBuildable(Arg.Any<string>()).Returns(f => new List<MaterialRequirementDTO>
            {
                new MaterialRequirementDTO
                {
                    ItemID = Raven,
                    MaterialID = Tritanium,
                    BaseQuantity = 1
                },
                new MaterialRequirementDTO
                {
                    ItemID = MinmatarFuelBlock,
                    MaterialID = NitrogenIsotopes,
                    BaseQuantity = 100
                }
            });
        }

        private static IMarketPriceService CreatePrices()
        {
            var prices = Substitute.For<IMarketPriceService>();
            prices.List(Arg.Any<MarketPriceRequest>()).Returns(f => new List<MarketPriceDTO>
            {
                new MarketPriceDTO
                {
                    ItemID = Tritanium,
                    CurrentPrice = 1
                },
                new MarketPriceDTO
                {
                    ItemID = NitrogenIsotopes,
                    CurrentPrice = 1
                }
            });
            return prices;
        }


    }
}
