﻿using System;

namespace DBSoft.UI
{
	public abstract class ControllerAction
	{
		protected ControllerAction()
		{
			Click = DoAction;
		}

		public EventHandler Click { private set; get; }

		protected abstract void DoAction(object sender, EventArgs e);
	}
}