﻿namespace Dbsoft.Epm.Web.Controllers.Reporting.ItemSales
{
    using System;

    public class ItemSalesModel
    {
        public ItemSalesModel(DateTime? fromDate, DateTime? toDate)
        {
            FromDate = fromDate ?? DateTime.UtcNow.AddDays(-7);
            ToDate = toDate ?? DateTime.UtcNow.AddDays(-1);
        }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}