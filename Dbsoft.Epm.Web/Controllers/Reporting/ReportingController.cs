﻿namespace Dbsoft.Epm.Web.Controllers.Reporting
{
    using System;
    using System.Threading.Tasks;
    using DailySales;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.ItemCosts;
    using DBSoft.EPM.DAL.Services.Transactions;
    using Infrastructure;
    using ItemSales;
    using JetBrains.Annotations;
    using Microsoft.AspNet.Mvc;

    public class ReportingController : EpmController
    {
        private readonly IItemTransactionService _sales;
        private readonly IReportModelBuilder _reportModelBuilder;

        [UsedImplicitly]
        public ReportingController(IItemTransactionService sales, IItemCostService itemCostService, 
            IReportModelBuilder reportModelBuilder, IUserService users) : base(users)
        {
            _sales = sales;
            _reportModelBuilder = reportModelBuilder;
        }

        public ActionResult ItemCostTrend()
        {
            return View();
        }

        public ActionResult MaterialCostTrend()
        {
            return View();
        }

        public ActionResult ItemSales(DateTime? fromDate, DateTime? toDate)
        {
            return View(new ItemSalesModel(fromDate, toDate));
        }

        public ActionResult DailySales()
        {
            var model = new DailySaleModel(_sales, Token);
            return View(model);
        }

        public ActionResult MonthlySales()
        {
            return View();
        }

        public async Task<ActionResult> MarketResearch()
        {
            var model = await Task.Run(() => _reportModelBuilder.CreateMarketResearchModel(Token));
            return View(model);
        }

        public ActionResult SubscriberSales(DateTime? fromDate, DateTime? toDate)
        {
            return View(new ItemSalesModel(fromDate, toDate));
        }
    }
}
