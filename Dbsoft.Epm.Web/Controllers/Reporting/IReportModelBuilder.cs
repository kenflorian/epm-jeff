namespace Dbsoft.Epm.Web.Controllers.Reporting
{
    using System;

    public interface IReportModelBuilder
    {
        MarketResearchModel CreateMarketResearchModel(string token);
    }
}