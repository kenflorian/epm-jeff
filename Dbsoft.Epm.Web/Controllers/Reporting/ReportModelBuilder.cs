namespace Dbsoft.Epm.Web.Controllers.Reporting
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using DBSoft.EPM.DAL.Services.Market;
    using DBSoft.EPM.DAL.Services.Transactions;
    using DBSoft.EPMWeb.Models.Reporting;
    using JetBrains.Annotations;

    [UsedImplicitly]
    public class ReportModelBuilder : IReportModelBuilder
    {
        private readonly IMarketResearchService _service;
        private readonly ITransactionService _transactions;

        public ReportModelBuilder(IMarketResearchService service, ITransactionService transactions)
        {
            _service = service;
            _transactions = transactions;
            Mapper.CreateMap<MarketResearchDTO, MarketResearchDetailModel>()
                .ForMember(m => m.Margin, opt => opt.ResolveUsing(r => r.Price - r.Cost))
                .ForMember(m => m.Markup, opt => opt.ResolveUsing(r => r.Cost == 0 ? 0 : (r.Price - r.Cost) / r.Cost * 100))
                ;
        }

        public MarketResearchModel CreateMarketResearchModel(string token)
        {
            return new MarketResearchModel
            {
                Detail = Task.Run(() => _service.List(token))
                    .Result
                    .Select(Mapper.Map<MarketResearchDetailModel>)
                    .OrderByDescending(o => o.ProfitFactor).ToList()
            };
        }
    }
}