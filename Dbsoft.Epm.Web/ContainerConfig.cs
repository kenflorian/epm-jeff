namespace Dbsoft.Epm.Web
{
    using System.Collections.Generic;
    using Autofac;
    using Autofac.Core;
    using Autofac.Framework.DependencyInjection;
    using Infrastructure;
    using Microsoft.Framework.Configuration;
    using Microsoft.Framework.DependencyInjection;

    public class ContainerConfig
    {
        public static IContainer BuildContainer(IServiceCollection services, IConfiguration config, IEnumerable<IModule> modules = null)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new AutofacModule(config));

            if (config.Get("Mocks:UseEveApiMocks") == "true")
            {
                builder.RegisterModule(new MockModule());
            }
            if (modules != null)
            {
                foreach (var module in modules)
                {
                    builder.RegisterModule(module);
                }
            }
            builder.Populate(services);
            return builder.Build();
        }
    }
}