﻿namespace DBSoft.EPMWeb.Support.ModelBinders
{
	using System;
	using System.Web.Mvc;

	public class DateTimeBinder : IModelBinder
	{
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

			return value == null ? DateTime.Now : value.ConvertTo(typeof(DateTime), value.Culture);
		}
	}

	public class NullableDateTimeBinder : IModelBinder
	{
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

			if (value != null)
			{
				return value.ConvertTo(typeof(DateTime), value.Culture);
			}
			return null;
		}
	}
}