﻿namespace DBSoft.EPMWeb.Models.Reporting
{
    using EPM.UI;
    using EPM.DAL;
	using EPM.DAL.CodeFirst.Models;
	using EPM.DAL.DTOs;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using EPM.DAL.Services.Transactions;

	public class ItemTransactionModel
	{
		private readonly IEnumerable<ItemTransactionDTO> _detail;
		private readonly TableDefinition _tableDef;

		public ItemTransactionModel(IItemTransactionService service, int itemID, DateTime fromDate, DateTime toDate, string token)
		{
			_detail = service
				.List(new ItemTransactionRequest 
				{ 
					Token = token,
					ItemID = itemID, 
					DateRange = new DateRange(fromDate, toDate), 
					TransactionType = TransactionType.Sell
				})
				.OrderByDescending(f => f.DateTime);

			_tableDef = new TableDefinition
			{
				Columns = new List<IColumnDefinition> 
				{ 
					new DataColumnDefinition<ItemTransactionDTO>(f => f.ItemName),
					new DataColumnDefinition<ItemTransactionDTO>(f => f.DateTime, caption: "Date/Time"),
					new DataColumnDefinition<ItemTransactionDTO>(f => f.Quantity),
					new DataColumnDefinition<ItemTransactionDTO>(f => f.Price),
					new DataColumnDefinition<ItemTransactionDTO>(f => f.Cost),
					new DataColumnDefinition<ItemTransactionDTO>(f => f.GPPct),
				}
			};
		}

		public string TableHtml
		{
			get
			{
				return TableHtmlGenerator.Generate(_tableDef, _detail);
			}
		}
	}
}