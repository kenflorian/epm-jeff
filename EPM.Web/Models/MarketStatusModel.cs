﻿namespace DBSoft.EPMWeb.Models
{
	using EPMWeb;

	public class MarketStatusModel : IImportStatusModel
	{
		public string ButtonText
		{
			get
			{
				return Callback.Status == ImportStatus.Ready ? ButtonReadyText : ButtonActiveText;
			}
		}

		public string ActionName { get { return "StartMarketRefresh"; } }
		public string ButtonReadyText { get { return "Refresh"; } }
		public string ButtonActiveText { get { return "Refreshing"; } }
		public ImportCallback Callback { get; set; }
		public string ButtonId { get { return "market-button"; } }
	}
}