﻿namespace DBSoft.EPMWeb.Controllers
{
    using System.Linq;
    using System.Threading.Tasks;
    using Annotations;
    using System;
    using AutoMapper;
    using EPM.DAL.DTOs;
    using EPM.DAL.Interfaces;
    using EPM.DAL.Requests;
    using EPM.DAL.Services.Contracts;
    using EPM.Logic;
    using System.Web.Mvc;
    using Models.Production;
    using Models.Reporting;
    using Support.Filters;

    [RequireDatabaseAuthentication]
    public class ProductionController : EpmController
    {
        private readonly IMarketRestockService _marketRestockService;
        private readonly IProductionQueueService _productionQueueService;
        private readonly IProductionMaterialService _productionMaterialService;
        private readonly IItemService _itemService;
        private readonly IConfigurationService _config;
        private readonly IMarketRepriceService _marketRepriceService;
        private readonly IMaterialPurchaseService _materialPurchaseService;
        private readonly IMaterialItemService _materialItemService;
        private readonly IImportManager _importer;
        private readonly IContractService _contractService;

        [UsedImplicitly]
        public ProductionController(IMarketRestockService marketRestockService, IProductionQueueService productionQueueService,
            IProductionMaterialService productionMaterialService, IItemService itemService,
            IConfigurationService config, IMarketRepriceService marketRepriceService,
            IMaterialPurchaseService materialPurchaseService, IMaterialItemService materialItemService, IEpmConfig settings,
            IContractService contractService, IImportManager importer)
        {
            _marketRestockService = marketRestockService;
            _productionQueueService = productionQueueService;
            _productionMaterialService = productionMaterialService;
            _itemService = itemService;
            _config = config;
            _marketRepriceService = marketRepriceService;
            _materialPurchaseService = materialPurchaseService;
            _materialItemService = materialItemService;
            _importer = importer;
            _contractService = contractService;
            Mapper.CreateMap<MaterialItemDTO, MaterialItemModel>();

        }
        public ActionResult Index()
        {
            return View();
        }

        [RequireCompleteConfiguration]
        public ActionResult RefreshApi()
        {
            var model = new RefreshApiModel();
            return View(model);
        }

        [HttpPost]
        public JsonResult StartApiRefresh()
        {
            Task.Run(() => _importer.StartImport(Token));
            return Json("OK");
        }

        public JsonNetResult GetApiStatus()
        {
            var items = _importer.List(Token).OrderBy(f => f.ApiName).ToList();
            return new JsonNetResult
            {
                Data = new { status = "OK", data = items },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult MarketRestock()
        {
            return View(new PostSellOrdersModel(_marketRestockService, Token));
        }

        public ActionResult MarketReprice()
        {
            var model = new UpdateSellOrderModel(_marketRepriceService, Token);
            return View(model);
        }

        public ActionResult ItemBuild()
        {
            return View(new ProductionQueueModel(_productionQueueService, Token));
        }

        public ActionResult ItemMaintenance()
        {
            return View();
        }

        public ActionResult BuildableItemList()
        {
            var model = new BuildableItemListModel(_itemService, Token, System.Web.HttpContext.Current.Request.Url.PathAndQuery);
            return PartialView(model);
        }

        public ActionResult MaterialItem(int itemID, string returnUrl)
        {
            var item = _materialItemService
                .List(Token)
                .Single(f => f.ItemID == itemID);
            var model = Mapper.Map<MaterialItemModel>(item);
            model.ReturnUrl = returnUrl;
            return View("MaterialItem", model);
        }

        [HttpPost]
        public ActionResult MaterialItem(MaterialItemModel item)
        {
            _itemService.UpdateMaterial(new UpdateMaterialRequest
            {
                Token = Token,
                ItemID = item.ItemID,
                BounceFactor = item.BounceFactor
            });
            return Redirect(item.ReturnUrl);
        }

        [HttpPost]
        public JsonResult SaveItem(BuildableItemModel model)
        {
            try
            {
                _itemService.UpdateItem(new UpdateItemRequest
                {
                    ItemID = model.ItemID,
                    Token = Token,
                    MinimumStock = model.MinimumStock,
                    PerJobAdditionalCost = model.PerJobAdditionalCost
                });
                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception e)
            {
                var result = Json(new { Success = false, e.Message });
                return result;
            }

        }

        [HttpPost]
        public JsonResult SaveMaterial(MaterialItemModel model)
        {
            try
            {
                _itemService.UpdateMaterial(new UpdateMaterialRequest
                {
                    ItemID = model.ItemID,
                    Token = Token,
                    BounceFactor = model.BounceFactor
                });
                return Json(new
                {
                    Success = true,
                    LastModified = DateTime.UtcNow.ToString("d")
                });
            }
            catch (Exception e)
            {
                var result = Json(new { Success = false, e.Message });
                return result;
            }

        }

        public ActionResult MaterialItemList()
        {
            var model = new MaterialItemListModel(_materialItemService, Token, System.Web.HttpContext.Current.Request.Url.PathAndQuery);
            return PartialView(model);
        }

        public ActionResult PurchaseRelist()
        {
            var model = new PostBuyOrderModel(_materialPurchaseService, Token, IsEveClient);
            return View(model);
        }

        public ActionResult PurchaseReprice()
        {
            var model = new UpdateBuyOrderModel(_materialPurchaseService, Token);
            return View(model);
        }

        public ActionResult InboundContract()
        {
            var model = new InboundContractModel(_contractService, Token);
            return View(model);
        }

        public ActionResult OutboundContract()
        {
            var model = new OutboundContractModel(_contractService, _config, Token);
            return View(model);
        }

        public ActionResult ProductionMaterial()
        {
            return View(new ProductionMaterialModel(_productionMaterialService, Token,
                System.Web.HttpContext.Current.Request.Url.PathAndQuery));

        }
    }
}
