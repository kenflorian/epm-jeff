﻿namespace DBSoft.EPMWeb.Controllers
{
    using System.Threading.Tasks;
    using Annotations;
    using EPM.DAL.Services.AccountApi;
    using AutoMapper;
    using EPM.DAL.DTOs;
    using EPM.DAL.Interfaces;
	using EPM.DAL.Requests;
    using EPM.Logic;
    using Models.Account;
	using Models.Maintenance;
	using Support.Filters;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web.Mvc;

	[RequireDatabaseAuthentication]
	public class MaintenanceController : EpmController
	{
		private readonly IConfigurationService _config;
	    private readonly IConfigurationProcessor _configProcessor;
		private readonly IUniverseService _universeService;
	    private readonly IAccountService _accounts;
	    private readonly IEveAccountProcessor _accountProcessor;

	    [UsedImplicitly]
	    public MaintenanceController(IConfigurationProcessor configurationProcessor, 
            IUniverseService universeService, IAccountService accounts, IEveAccountProcessor accountProcessor,
            IConfigurationService config)
		{
            _config = config;
	        _configProcessor = configurationProcessor;
            _universeService = universeService;
		    _accounts = accounts;
	        _accountProcessor = accountProcessor;

	        Mapper.CreateMap<ConfigurationModel, ConfigurationSettingsDTO>()
				.ForMember(m => m.FactoryLocation, opt => opt.MapFrom(m => m.FactoryID))
				.ForMember(m => m.MarketSellLocation, opt => opt.MapFrom(m => m.MarketSellID))
                .ForMember(m => m.MarketBuyLocation, opt => opt.MapFrom(m => m.MarketBuyID))
                .ForMember(m => m.PosLocation, opt => opt.MapFrom(m => m.PosLocationID));
			Mapper.CreateMap<ConfigurationSettingsDTO, ConfigurationModel>()
				.ForMember(m => m.FactoryID, opt => opt.MapFrom(m => m.FactoryLocation))
				.ForMember(m => m.MarketSellID, opt => opt.MapFrom(m => m.MarketSellLocation))
                .ForMember(m => m.MarketBuyID, opt => opt.MapFrom(m => m.MarketBuyLocation))
                .ForMember(m => m.PosLocationID, opt => opt.MapFrom(m => m.PosLocation));
			Mapper.CreateMap<AccountDTO, AccountModel>().ReverseMap();
		}

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Configuration()
		{
			var settings = _config.List(Token);
			var model = Mapper.Map<ConfigurationModel>(settings);
			model.ConfigurationValid = _configProcessor.ConfigurationIsValid(Token);

			return View(model);
		}

		[HttpPost]
		public JsonResult Configuration(ConfigurationModel config)
		{
			var settings = Mapper.Map<ConfigurationSettingsDTO>(config);
			_config.SaveSettings(Token, settings);
			return JsonResponse(null);
		}

		public ActionResult Accounts()
		{
			var model = new AccountsModel
				{
					Accounts = Mapper.Map<IEnumerable<AccountDTO>, List<AccountModel>>(
						_accounts.List(new AccountRequest {Token = Token, IncludeDeleted = true}))
				};
			return View(model);
		}

		[HttpPost]
		public async Task<JsonResult> SaveAccount(AccountModel model)
		{
			try
			{
				var account = Mapper.Map<AccountDTO>(model);
				var result = await _accountProcessor.SaveAccount(account, Token);
				return Json(new
				{
					Success = true,
					Model = result
				});
			}
			catch (Exception e)
			{
				var result = Json(new { Success = false, e.Message });
				return result;
			}
		}

		private JsonResult JsonResponse(dynamic model)
		{
			return Json(new
				{
					Success = true,
					Model = model
				}, JsonRequestBehavior.AllowGet);
		}

		public async Task<JsonResult> RegionLookup(int regionId)
		{
			return JsonResponse((await _universeService
							.ListRegions())
							.SingleOrDefault(f => f.RegionID == regionId));
		}

		public JsonResult StationLookup(int stationId)
		{
            var result = _universeService
                        .ListStations()
                        .SingleOrDefault(f => f.StationID == stationId);
			return JsonResponse(result);
		}

        public JsonResult StationList(string stationPartial)
		{
			return JsonResponse(_universeService
						.ListStations()
						.Where(f => f.StationName.StartsWith(stationPartial, StringComparison.InvariantCultureIgnoreCase))
						.OrderBy(f => f.StationName));
		}

        public JsonResult SolarSystemList(string systemPartial)
	    {
            return JsonResponse(_universeService.ListSolarSystems()
                .Where(f => f.SolarSystemName.StartsWith(systemPartial, StringComparison.InvariantCultureIgnoreCase))
                .OrderBy(f => f.SolarSystemName));
	    }

        public JsonResult SolarSystemLookup(int? solarSystemId)
        {
            return JsonResponse(_universeService
                .ListSolarSystems()
                .SingleOrDefault((f => f.SolarSystemID == solarSystemId)));
        }
	}
}
