USE [master]
GO
/****** Object:  Database [EPM]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE DATABASE [EPM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EPM_Data', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\EPM.mdf' , SIZE = 523328KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EPM_Log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\EPM.ldf' , SIZE = 76736KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EPM] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EPM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EPM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EPM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EPM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EPM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EPM] SET ARITHABORT OFF 
GO
ALTER DATABASE [EPM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EPM] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [EPM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EPM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EPM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EPM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EPM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EPM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EPM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EPM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EPM] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EPM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EPM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EPM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EPM] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [EPM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EPM] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [EPM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EPM] SET RECOVERY FULL 
GO
ALTER DATABASE [EPM] SET  MULTI_USER 
GO
ALTER DATABASE [EPM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EPM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EPM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EPM] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [EPM]
GO
/****** Object:  StoredProcedure [dbo].[RepairAllViews]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RepairAllViews]
AS
BEGIN

DECLARE @getView CURSOR
DECLARE @ViewName VARCHAR(40)
SET @getView = CURSOR FOR
SELECT SCHEMA_NAME(schema_id) + '.' + name
FROM sys.views where SCHEMA_NAME(schema_id) = 'dbo'
OPEN @getView
FETCH NEXT
FROM @getView INTO @ViewName
WHILE @@FETCH_STATUS = 0
BEGIN

BEGIN TRY
EXEC sys.sp_refreshsqlmodule @ViewName
END TRY
BEGIN CATCH
PRINT @ViewName
PRINT ERROR_MESSAGE()
ROLLBACK
END CATCH

FETCH NEXT
FROM @getView INTO @ViewName
END
CLOSE @getView
DEALLOCATE @getView


END
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Account]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[ID] [int] IDENTITY(3,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[ApiKeyID] [int] NOT NULL,
	[ApiVerificationCode] [varchar](65) NOT NULL,
	[ApiKeyType] [int] NOT NULL,
	[ApiAccessMask] [int] NOT NULL,
	[DeletedFlag] [bit] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountBalance]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountBalance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[AccountKey] [int] NOT NULL,
	[Balance] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_AccountBalance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ApplicationLog]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicationLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Origin] [nvarchar](max) NOT NULL,
	[LogLevel] [nvarchar](50) NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[Exception] [varchar](max) NULL,
	[StackTrace] [varchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[Application] [nvarchar](max) NULL,
 CONSTRAINT [PK_ApplicationLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Asset]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asset](
	[ID] [bigint] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[AccountID] [int] NOT NULL,
	[ContainerID] [int] NULL,
	[DeletedFlag] [bit] NOT NULL,
	[StationID] [int] NULL,
	[LocationID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[SolarSystemID] [int] NULL,
 CONSTRAINT [PK_Asset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Blueprint]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blueprint](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[ProductionTime] [int] NOT NULL,
	[BuildItemID] [int] NULL,
 CONSTRAINT [PK_Blueprint] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BlueprintInstance]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlueprintInstance](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AssetID] [bigint] NOT NULL,
	[MaterialEfficiency] [int] NOT NULL,
	[ProductionEfficiency] [int] NOT NULL,
	[DeletedFlag] [bit] NOT NULL,
	[BlueprintID] [bigint] NOT NULL,
	[IsCopy] [bit] NOT NULL,
 CONSTRAINT [PK_BlueprintInstance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Character]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Character](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[AccountID] [int] NOT NULL,
	[CorporationID] [int] NOT NULL,
	[EveApiID] [int] NOT NULL,
 CONSTRAINT [PK_Character] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Configuration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [varchar](50) NOT NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_Parameters] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Container]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Container](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Container] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Corporation]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Corporation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[EveApiID] [int] NOT NULL,
	[AccountID] [int] NULL,
 CONSTRAINT [PK_Corporation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EveAccount]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EveAccount](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EveCharacterName] [nvarchar](50) NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.EveAccount] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EveApiStatus]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EveApiStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CacheExpiry] [datetime] NOT NULL,
	[UserID] [int] NOT NULL,
	[Result] [varchar](max) NULL,
 CONSTRAINT [PK_EveApiStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Gate]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SolarSystemID] [int] NOT NULL,
	[TargetSolarSystemID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Gate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Group]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[CategoryID] [int] NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HiredTeam]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HiredTeam](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeamID] [int] NOT NULL,
	[HireDate] [datetime] NOT NULL,
	[HireAmount] [decimal](18, 2) NOT NULL,
	[MaterialBonusValue] [decimal](18, 2) NOT NULL,
	[Salary] [decimal](18, 2) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Description2] [nvarchar](max) NULL,
	[SpecialtyNames] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.HiredTeam] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HiredTeamAuction]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HiredTeamAuction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HiredTeamID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[AuctionCost] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_dbo.HiredTeamAuction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HiredTeamCost]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HiredTeamCost](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HiredTeamID] [int] NOT NULL,
	[TransactionID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.HiredTeamCost] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HiredTeamSpecialty]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HiredTeamSpecialty](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HiredTeamID] [int] NOT NULL,
	[SpecialtyName] [nvarchar](max) NULL,
	[Bonus] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_dbo.HiredTeamSpecialty] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IpAddressBlacklist]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IpAddressBlacklist](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IpAddress] [varchar](50) NOT NULL,
	[Count] [int] NOT NULL,
 CONSTRAINT [PK_IpAddressBlacklist] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Item]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[ID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[GroupID] [int] NOT NULL,
	[QuantityMultiplier] [int] NOT NULL,
	[ItemMetaGroup] [int] NULL,
	[IsPublished] [bit] NOT NULL,
	[Volume] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemExtension]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemExtension](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MinimumStock] [int] NULL,
	[BounceFactor] [decimal](16, 2) NULL,
	[ItemID] [int] NOT NULL,
	[UserID] [int] NULL,
	[PerJobAdditionalCost] [decimal](18, 2) NULL,
	[LastModified] [datetime] NULL,
 CONSTRAINT [PK_ItemExtension] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Manifest]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manifest](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[MaterialItemID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[AdditionalQuantity] [int] NULL,
 CONSTRAINT [PK_Manifest] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarketImport]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketImport](
	[ID] [bigint] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[StationID] [int] NOT NULL,
	[SolarSystemID] [int] NOT NULL,
	[RegionID] [int] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[ItemID] [int] NOT NULL,
	[OrderType] [int] NOT NULL,
	[Jumps] [smallint] NOT NULL,
	[Range] [smallint] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_MarketImport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarketImport2]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketImport2](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[StationID] [int] NOT NULL,
	[SolarSystemID] [int] NOT NULL,
	[RegionID] [int] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[ItemID] [int] NOT NULL,
	[OrderType] [int] NOT NULL,
	[Jumps] [smallint] NOT NULL,
	[Range] [smallint] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.MarketImport2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarketOrder]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EveMarketOrderID] [bigint] NOT NULL,
	[ItemID] [int] NOT NULL,
	[EveCharacterID] [int] NOT NULL,
	[StationID] [int] NOT NULL,
	[OriginalQuantity] [int] NOT NULL,
	[RemainingQuantity] [int] NOT NULL,
	[MinimumQuantity] [int] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[Range] [int] NULL,
	[Duration] [int] NOT NULL,
	[Escrow] [decimal](16, 2) NOT NULL,
	[Price] [decimal](16, 2) NOT NULL,
	[OrderType] [int] NULL,
	[WhenIssued] [datetime] NOT NULL,
	[CharacterID] [int] NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_MarketOrder_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarketPrice]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketPrice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NOT NULL,
	[AdjustedPrice] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_dbo.MarketPrice] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarketResearch]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketResearch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StationID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemName] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Cost] [decimal](18, 2) NOT NULL,
	[ProductionTime] [time](7) NOT NULL,
	[Inventory] [bigint] NOT NULL,
	[Volume] [float] NOT NULL,
	[Competitors] [int] NOT NULL,
	[QuantityMultiplier] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_dbo.MarketResearch] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductionJob]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionJob](
	[ID] [bigint] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[WhenInstalled] [datetime] NOT NULL,
	[WhenStarted] [datetime] NULL,
	[WhenCompleted] [datetime] NULL,
	[WhenPaused] [datetime] NULL,
	[AssetID] [bigint] NULL,
	[TeamSavings] [decimal](18, 2) NOT NULL,
	[HiredTeamID] [int] NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_ProductionJob] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Region]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Region](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchemaVersions]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchemaVersions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScriptName] [nvarchar](255) NOT NULL,
	[Applied] [datetime] NOT NULL,
 CONSTRAINT [PK_SchemaVersions_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sessions]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sessions](
	[SessionId] [nvarchar](88) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Expires] [datetime] NOT NULL,
	[LockDate] [datetime] NOT NULL,
	[LockCookie] [int] NOT NULL,
	[Locked] [bit] NOT NULL,
	[SessionItem] [varbinary](max) NULL,
	[Flags] [int] NOT NULL,
	[Timeout] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SolarSystem]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolarSystem](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[RegionID] [int] NOT NULL,
	[ManufacturingCost] [decimal](16, 4) NOT NULL,
	[InstallationTax] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_SolarSystem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Station]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Station](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SolarSystemID] [int] NOT NULL,
	[Tax] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Station] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[Quantity] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Price] [decimal](16, 2) NOT NULL,
	[TransactionType] [smallint] NOT NULL,
	[EveCharacterID] [int] NOT NULL,
	[CorporationID] [int] NULL,
	[Cost] [decimal](16, 2) NULL,
	[EveTransactionID] [bigint] NOT NULL,
	[UserID] [int] NOT NULL,
	[AccountID] [int] NULL,
	[VisibleFlag] [bit] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Password] [nvarchar](50) NULL,
	[AuthenticationFailures] [int] NULL,
	[LockedUntil] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[EveOnlineCharacter] [nvarchar](max) NULL,
	[SsoRefreshToken] [nvarchar](max) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserWhitelist]    Script Date: 15/01/2016 6:29:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserWhitelist](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_UserWhitelist] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[Account]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_AccountID] ON [dbo].[AccountBalance]
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_AccountID] ON [dbo].[Asset]
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[Asset]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SolarSystemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_SolarSystemID] ON [dbo].[Asset]
(
	[SolarSystemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[Asset]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_BuildItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_BuildItemID] ON [dbo].[Blueprint]
(
	[BuildItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[Blueprint]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_BlueprintID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_BlueprintID] ON [dbo].[BlueprintInstance]
(
	[BlueprintID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ_BlueprintInstance_Asset]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_BlueprintInstance_Asset] ON [dbo].[BlueprintInstance]
(
	[AssetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AccountID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_AccountID] ON [dbo].[Character]
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[Configuration]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[EveAccount]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[EveApiStatus]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SolarSystemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_SolarSystemID] ON [dbo].[Gate]
(
	[SolarSystemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TargetSolarSystemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_TargetSolarSystemID] ON [dbo].[Gate]
(
	[TargetSolarSystemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CategoryID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_CategoryID] ON [dbo].[Group]
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HiredTeamID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_HiredTeamID] ON [dbo].[HiredTeamAuction]
(
	[HiredTeamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[HiredTeamAuction]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HiredTeamID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_HiredTeamID] ON [dbo].[HiredTeamCost]
(
	[HiredTeamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TransactionID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_TransactionID] ON [dbo].[HiredTeamCost]
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HiredTeamID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_HiredTeamID] ON [dbo].[HiredTeamSpecialty]
(
	[HiredTeamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_GroupID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_GroupID] ON [dbo].[Item]
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[ItemExtension]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[ItemExtension]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[Manifest]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Manifest_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_Manifest_ItemID] ON [dbo].[Manifest]
(
	[ItemID] ASC
)
INCLUDE ( 	[MaterialItemID],
	[Quantity]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MaterialItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MaterialItemID] ON [dbo].[Manifest]
(
	[MaterialItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MarketImport_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MarketImport_ItemID] ON [dbo].[MarketImport]
(
	[ItemID] ASC
)
INCLUDE ( 	[TimeStamp]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MarketImport_ItemID_OrderType]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MarketImport_ItemID_OrderType] ON [dbo].[MarketImport]
(
	[ItemID] ASC,
	[OrderType] ASC
)
INCLUDE ( 	[TimeStamp],
	[StationID],
	[Price],
	[Jumps],
	[Range]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MarketImport_OrderType]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MarketImport_OrderType] ON [dbo].[MarketImport]
(
	[OrderType] ASC
)
INCLUDE ( 	[TimeStamp],
	[StationID],
	[Price],
	[ItemID],
	[Jumps],
	[Range]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MarketImport_StationID_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MarketImport_StationID_ItemID] ON [dbo].[MarketImport]
(
	[StationID] ASC,
	[ItemID] ASC
)
INCLUDE ( 	[TimeStamp]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MarketImport_StationID_OrderType]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MarketImport_StationID_OrderType] ON [dbo].[MarketImport]
(
	[StationID] ASC,
	[OrderType] ASC
)
INCLUDE ( 	[TimeStamp],
	[Price],
	[ItemID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MarketImport_UserID_TimeStamp]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MarketImport_UserID_TimeStamp] ON [dbo].[MarketImport]
(
	[UserID] ASC,
	[TimeStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[MarketImport2]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MarketImport2_Composite]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_MarketImport2_Composite] ON [dbo].[MarketImport2]
(
	[TimeStamp] ASC,
	[UserID] ASC,
	[OrderType] ASC,
	[StationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[MarketImport2]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[MarketOrder]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[MarketOrder]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[MarketPrice]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssetID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_AssetID] ON [dbo].[ProductionJob]
(
	[AssetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HiredTeamID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_HiredTeamID] ON [dbo].[ProductionJob]
(
	[HiredTeamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ItemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_ItemID] ON [dbo].[ProductionJob]
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Status]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_Status] ON [dbo].[ProductionJob]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserID] ON [dbo].[ProductionJob]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Sessions_Expires]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_Sessions_Expires] ON [dbo].[Sessions]
(
	[Expires] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_RegionID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_RegionID] ON [dbo].[SolarSystem]
(
	[RegionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SolarSystemID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_SolarSystemID] ON [dbo].[Station]
(
	[SolarSystemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EveTransactionID]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_EveTransactionID] ON [dbo].[Transaction]
(
	[EveTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Transaction_DateTime]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_Transaction_DateTime] ON [dbo].[Transaction]
(
	[DateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Transaction_Item]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_Transaction_Item] ON [dbo].[Transaction]
(
	[ItemID] ASC,
	[UserID] ASC,
	[VisibleFlag] ASC,
	[DateTime] ASC
)
INCLUDE ( 	[Quantity],
	[TransactionType],
	[Cost]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Transaction_User]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_Transaction_User] ON [dbo].[Transaction]
(
	[UserID] ASC,
	[VisibleFlag] ASC,
	[DateTime] ASC
)
INCLUDE ( 	[Quantity],
	[ItemID],
	[TransactionType]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TransactionType]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE NONCLUSTERED INDEX [IX_TransactionType] ON [dbo].[Transaction]
(
	[TransactionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_Login]    Script Date: 15/01/2016 6:29:11 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Login] ON [dbo].[UserWhitelist]
(
	[Login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_DeletedFlag]  DEFAULT ((0)) FOR [DeletedFlag]
GO
ALTER TABLE [dbo].[ApplicationLog] ADD  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[Asset] ADD  CONSTRAINT [DF_Asset_DeletedFlag]  DEFAULT ((0)) FOR [DeletedFlag]
GO
ALTER TABLE [dbo].[BlueprintInstance] ADD  CONSTRAINT [DF_BlueprintInstance_DeletedFlag]  DEFAULT ((0)) FOR [DeletedFlag]
GO
ALTER TABLE [dbo].[BlueprintInstance] ADD  CONSTRAINT [DF_BlueprintInstance_IsCopy]  DEFAULT ((0)) FOR [IsCopy]
GO
ALTER TABLE [dbo].[HiredTeam] ADD  DEFAULT ((0)) FOR [MaterialBonusValue]
GO
ALTER TABLE [dbo].[HiredTeam] ADD  DEFAULT ((0)) FOR [Salary]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [IsPublished]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [Volume]
GO
ALTER TABLE [dbo].[MarketResearch] ADD  DEFAULT ((0)) FOR [Competitors]
GO
ALTER TABLE [dbo].[MarketResearch] ADD  DEFAULT ((0)) FOR [QuantityMultiplier]
GO
ALTER TABLE [dbo].[ProductionJob] ADD  DEFAULT ((0)) FOR [TeamSavings]
GO
ALTER TABLE [dbo].[SolarSystem] ADD  DEFAULT ((0)) FOR [ManufacturingCost]
GO
ALTER TABLE [dbo].[SolarSystem] ADD  DEFAULT ((0)) FOR [InstallationTax]
GO
ALTER TABLE [dbo].[Station] ADD  DEFAULT ((0)) FOR [Tax]
GO
ALTER TABLE [dbo].[Transaction] ADD  DEFAULT ((1)) FOR [VisibleFlag]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_User]
GO
ALTER TABLE [dbo].[AccountBalance]  WITH CHECK ADD  CONSTRAINT [FK_AccountBalance_Account] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountBalance] CHECK CONSTRAINT [FK_AccountBalance_Account]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Account] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_Account]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_Item]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_User]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Asset_dbo.SolarSystem_SolarSystemID] FOREIGN KEY([SolarSystemID])
REFERENCES [dbo].[SolarSystem] ([ID])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_dbo.Asset_dbo.SolarSystem_SolarSystemID]
GO
ALTER TABLE [dbo].[Blueprint]  WITH CHECK ADD  CONSTRAINT [FK_Blueprint_BuildItem] FOREIGN KEY([BuildItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[Blueprint] CHECK CONSTRAINT [FK_Blueprint_BuildItem]
GO
ALTER TABLE [dbo].[Blueprint]  WITH CHECK ADD  CONSTRAINT [FK_Blueprint_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[Blueprint] CHECK CONSTRAINT [FK_Blueprint_Item]
GO
ALTER TABLE [dbo].[BlueprintInstance]  WITH CHECK ADD  CONSTRAINT [FK_BlueprintInstance_Asset] FOREIGN KEY([AssetID])
REFERENCES [dbo].[Asset] ([ID])
GO
ALTER TABLE [dbo].[BlueprintInstance] CHECK CONSTRAINT [FK_BlueprintInstance_Asset]
GO
ALTER TABLE [dbo].[BlueprintInstance]  WITH CHECK ADD  CONSTRAINT [FK_BlueprintInstance_Blueprint] FOREIGN KEY([BlueprintID])
REFERENCES [dbo].[Blueprint] ([ID])
GO
ALTER TABLE [dbo].[BlueprintInstance] CHECK CONSTRAINT [FK_BlueprintInstance_Blueprint]
GO
ALTER TABLE [dbo].[Character]  WITH CHECK ADD  CONSTRAINT [FK_Character_Account] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Character] CHECK CONSTRAINT [FK_Character_Account]
GO
ALTER TABLE [dbo].[Configuration]  WITH CHECK ADD  CONSTRAINT [FK_Configuration_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Configuration] CHECK CONSTRAINT [FK_Configuration_User]
GO
ALTER TABLE [dbo].[EveAccount]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EveAccount_dbo.User_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[EveAccount] CHECK CONSTRAINT [FK_dbo.EveAccount_dbo.User_UserID]
GO
ALTER TABLE [dbo].[EveApiStatus]  WITH CHECK ADD  CONSTRAINT [FK_EveApiStatus_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[EveApiStatus] CHECK CONSTRAINT [FK_EveApiStatus_User]
GO
ALTER TABLE [dbo].[Gate]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Gate_dbo.SolarSystem_SolarSystemID] FOREIGN KEY([SolarSystemID])
REFERENCES [dbo].[SolarSystem] ([ID])
GO
ALTER TABLE [dbo].[Gate] CHECK CONSTRAINT [FK_dbo.Gate_dbo.SolarSystem_SolarSystemID]
GO
ALTER TABLE [dbo].[Gate]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Gate_dbo.SolarSystem_TargetSolarSystemID] FOREIGN KEY([TargetSolarSystemID])
REFERENCES [dbo].[SolarSystem] ([ID])
GO
ALTER TABLE [dbo].[Gate] CHECK CONSTRAINT [FK_dbo.Gate_dbo.SolarSystem_TargetSolarSystemID]
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK_Group_Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK_Group_Category]
GO
ALTER TABLE [dbo].[HiredTeamAuction]  WITH CHECK ADD  CONSTRAINT [FK_dbo.HiredTeamAuction_dbo.HiredTeam_HiredTeamID] FOREIGN KEY([HiredTeamID])
REFERENCES [dbo].[HiredTeam] ([ID])
GO
ALTER TABLE [dbo].[HiredTeamAuction] CHECK CONSTRAINT [FK_dbo.HiredTeamAuction_dbo.HiredTeam_HiredTeamID]
GO
ALTER TABLE [dbo].[HiredTeamAuction]  WITH CHECK ADD  CONSTRAINT [FK_dbo.HiredTeamAuction_dbo.User_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[HiredTeamAuction] CHECK CONSTRAINT [FK_dbo.HiredTeamAuction_dbo.User_UserID]
GO
ALTER TABLE [dbo].[HiredTeamCost]  WITH CHECK ADD  CONSTRAINT [FK_dbo.HiredTeamCost_dbo.HiredTeam_HiredTeamID] FOREIGN KEY([HiredTeamID])
REFERENCES [dbo].[HiredTeam] ([ID])
GO
ALTER TABLE [dbo].[HiredTeamCost] CHECK CONSTRAINT [FK_dbo.HiredTeamCost_dbo.HiredTeam_HiredTeamID]
GO
ALTER TABLE [dbo].[HiredTeamCost]  WITH CHECK ADD  CONSTRAINT [FK_dbo.HiredTeamCost_dbo.Transaction_TransactionID] FOREIGN KEY([TransactionID])
REFERENCES [dbo].[Transaction] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[HiredTeamCost] CHECK CONSTRAINT [FK_dbo.HiredTeamCost_dbo.Transaction_TransactionID]
GO
ALTER TABLE [dbo].[HiredTeamSpecialty]  WITH CHECK ADD  CONSTRAINT [FK_dbo.HiredTeamSpecialty_dbo.HiredTeam_HiredTeamID] FOREIGN KEY([HiredTeamID])
REFERENCES [dbo].[HiredTeam] ([ID])
GO
ALTER TABLE [dbo].[HiredTeamSpecialty] CHECK CONSTRAINT [FK_dbo.HiredTeamSpecialty_dbo.HiredTeam_HiredTeamID]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Group] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Group] ([ID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Group]
GO
ALTER TABLE [dbo].[ItemExtension]  WITH CHECK ADD  CONSTRAINT [FK_ItemExtension_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[ItemExtension] CHECK CONSTRAINT [FK_ItemExtension_Item]
GO
ALTER TABLE [dbo].[ItemExtension]  WITH CHECK ADD  CONSTRAINT [FK_ItemExtension_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ItemExtension] CHECK CONSTRAINT [FK_ItemExtension_User]
GO
ALTER TABLE [dbo].[Manifest]  WITH CHECK ADD  CONSTRAINT [FK_Manifest_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[Manifest] CHECK CONSTRAINT [FK_Manifest_Item]
GO
ALTER TABLE [dbo].[Manifest]  WITH CHECK ADD  CONSTRAINT [FK_Manifest_Material] FOREIGN KEY([MaterialItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[Manifest] CHECK CONSTRAINT [FK_Manifest_Material]
GO
ALTER TABLE [dbo].[MarketImport]  WITH CHECK ADD  CONSTRAINT [FK_MarketImport_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[MarketImport] CHECK CONSTRAINT [FK_MarketImport_Item]
GO
ALTER TABLE [dbo].[MarketImport]  WITH CHECK ADD  CONSTRAINT [FK_MarketImport_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[MarketImport] CHECK CONSTRAINT [FK_MarketImport_User]
GO
ALTER TABLE [dbo].[MarketImport2]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MarketImport2_Item_ItemID] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[MarketImport2] CHECK CONSTRAINT [FK_dbo.MarketImport2_Item_ItemID]
GO
ALTER TABLE [dbo].[MarketImport2]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MarketImport2_User_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[MarketImport2] CHECK CONSTRAINT [FK_dbo.MarketImport2_User_UserID]
GO
ALTER TABLE [dbo].[MarketOrder]  WITH CHECK ADD  CONSTRAINT [FK_MarketOrder_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[MarketOrder] CHECK CONSTRAINT [FK_MarketOrder_Item]
GO
ALTER TABLE [dbo].[MarketOrder]  WITH CHECK ADD  CONSTRAINT [FK_MarketOrder_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[MarketOrder] CHECK CONSTRAINT [FK_MarketOrder_User]
GO
ALTER TABLE [dbo].[MarketPrice]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MarketPrice_dbo.Item_ItemID] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MarketPrice] CHECK CONSTRAINT [FK_dbo.MarketPrice_dbo.Item_ItemID]
GO
ALTER TABLE [dbo].[ProductionJob]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProductionJob_dbo.HiredTeam_HiredTeamID] FOREIGN KEY([HiredTeamID])
REFERENCES [dbo].[HiredTeam] ([ID])
GO
ALTER TABLE [dbo].[ProductionJob] CHECK CONSTRAINT [FK_dbo.ProductionJob_dbo.HiredTeam_HiredTeamID]
GO
ALTER TABLE [dbo].[ProductionJob]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProductionJob_dbo.User_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ProductionJob] CHECK CONSTRAINT [FK_dbo.ProductionJob_dbo.User_UserID]
GO
ALTER TABLE [dbo].[ProductionJob]  WITH CHECK ADD  CONSTRAINT [FK_ProductionJob_Asset] FOREIGN KEY([AssetID])
REFERENCES [dbo].[Asset] ([ID])
GO
ALTER TABLE [dbo].[ProductionJob] CHECK CONSTRAINT [FK_ProductionJob_Asset]
GO
ALTER TABLE [dbo].[ProductionJob]  WITH CHECK ADD  CONSTRAINT [FK_ProductionJob_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ID])
GO
ALTER TABLE [dbo].[ProductionJob] CHECK CONSTRAINT [FK_ProductionJob_Item]
GO
ALTER TABLE [dbo].[SolarSystem]  WITH CHECK ADD  CONSTRAINT [FK_SolarSystem_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[Region] ([ID])
GO
ALTER TABLE [dbo].[SolarSystem] CHECK CONSTRAINT [FK_SolarSystem_Region]
GO
ALTER TABLE [dbo].[Station]  WITH CHECK ADD  CONSTRAINT [FK_Station_SolarSystem] FOREIGN KEY([SolarSystemID])
REFERENCES [dbo].[SolarSystem] ([ID])
GO
ALTER TABLE [dbo].[Station] CHECK CONSTRAINT [FK_Station_SolarSystem]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_User]
GO
USE [master]
GO
ALTER DATABASE [EPM] SET  READ_WRITE 
GO
