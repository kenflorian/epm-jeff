﻿namespace Dbsoft.Epm.Test.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.AccountApi;
    using DBSoft.EPM.DAL.Services.ItemCosts;
    using DBSoft.EPM.DAL.Services.Materials;
    using DBSoft.EPM.DAL.Services.Transactions;
    using DBSoft.EPM.Logic;
    using DBSoft.EPM.Logic.Config;
    using DBSoft.EPM.Logic.RefreshApi;
    using DBSoft.EVEAPI.Crest;
    using DBSoft.EVEAPI.Crest.MarketOrder;
    using DBSoft.EVEAPI.Entities.Account;
    using DBSoft.EVEAPI.Entities.MarketOrder;
    using DBSoft.EVEAPI.Entities.WalletTransaction;
    using Microsoft.Framework.OptionsModel;
    using NSubstitute;
    using Xunit;

    public class RefreshApiTests
    {
        protected readonly IEveApiStatusService Status;
        protected IConfigurationService Config;

        public RefreshApiTests()
        {
            Status = Substitute.For<IEveApiStatusService>();
        }
    }


    public class TransactionMapperTests : RefreshApiTests
    {
        private readonly TransactionMapper _mapper;
        private readonly ITransactionService _transactions;
        private readonly IItemCostService _costs;
        private const int Raven = 638;
        private const int Rokh = 24688;

        public TransactionMapperTests()
        {
            _transactions = Substitute.For<ITransactionService>();
            var accounts = Substitute.For<IAccountApiService>();
            accounts.List(Arg.Any<string>()).Returns(new List<AccountApiDTO> {new AccountApiDTO()});
            var wallet = Substitute.For<IWalletTransactionService>();
            wallet.Load(ApiKeyType.Character, 0, "", 0, 0, 0)
                .ReturnsForAnyArgs(Task.Run(() => new ApiLoadResponse<WalletTransaction>
                {
                    Data = new List<WalletTransaction>
                    {
                        new WalletTransaction
                        {
                            Type = "sell",
                            TypeID = Raven
                        },
                        new WalletTransaction
                        {
                            Type = "sell",
                            TypeID = Rokh
                        }
                    }
                }));
            _costs = Substitute.For<IItemCostService>();
            _costs.ListBuildable(Arg.Any<ListBuildableRequest>()).Returns(new List<ItemCostDTO>
            {
                new ItemCostDTO(Config) { ItemID = Raven}
            });
            var items = Substitute.For<IItemService>();
            items.ListBuildable(Arg.Any<string>()).Returns(new List<BuildableItemDTO>
            {
                new BuildableItemDTO
                {
                    ItemID = Raven
                }
            });
            _mapper = new TransactionMapper(wallet, Status, _costs, accounts, _transactions, items);
        }

        [Fact]
        public async Task WhenCostNotFound_Throws()
        {
            _costs.ListBuildable(Arg.Any<ListBuildableRequest>()).Returns(new List<ItemCostDTO>());
            await Assert.ThrowsAsync<MapperException>(async () => { await _mapper.Pull(null); });
        }

        [Fact]
        public void WhenTrackedSale_SavesVisible()
        {
            _mapper.Pull("").Wait();
                _transactions.Received()
                    .SaveTransactions(Arg.Any<string>(),
                        Arg.Is<IEnumerable<SaveTransactionRequest>>(f => f.Any(a => a.ItemID == Raven && a.VisibleFlag)));
        }

        [Fact]
        public void WhenUntrackedSale_SavesHidden()
        {
            // Not sure why saving hidden untracked items. We need hidden buy items for costing, but the only real
            // concern here is that the Rokh doesn't show for whichever reason
            _mapper.Pull("").Wait();
            _transactions.Received()
                .SaveTransactions(Arg.Any<string>(),
                    Arg.Is<IEnumerable<SaveTransactionRequest>>(f => !f.Any(a => a.ItemID == Rokh && a.VisibleFlag)));
        }
    }


}
