﻿namespace Dbsoft.Epm.Test.Infrastructure
{
    using AutoMapper;
    using Web.Infrastructure;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.DTOs;
    using Xunit;

    public class AutomapperTests
    {
        [Fact]
        public void TestMappings()
        {
            MapperConfig.ConfigureMappings();

            Mapper.AssertConfigurationIsValid();

            Mapper.Map<MarketAdjustedPriceDTO>(new MarketPrice());
        }
    }
}
