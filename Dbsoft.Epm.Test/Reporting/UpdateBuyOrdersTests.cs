﻿namespace Dbsoft.Epm.Test.Reporting
{
    using System.Linq;
    using Autofac;
    using DBSoft.EPM.DAL.Extensions;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using Infrastructure;
    using Xunit;

    [Collection("Database collection")]
    public class UpdateBuyOrdersTests
    {
        [Fact]
        public void ListedMaterialsShouldBeDistinct()
        {
            var container = new IocContainerBuilder().Build();
            var service = container.Resolve<IMaterialPurchaseService>();
            var request = new MaterialPurchaseRequest
            {
                Token = DatabaseFixture.Token
            };
            var result =
                service.List(request).Where(f => f.ItemId == StaticData.Items.BuyOrderMultiMaterial.Id).ToList();
            Assert.True(result.Any());
            Assert.Equal(result.DistinctBy(f => f.ItemId).Count(), result.Count);
        }
    }
}
