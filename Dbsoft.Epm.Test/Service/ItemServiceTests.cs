﻿namespace Dbsoft.Epm.Test.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using DBSoft.EPM.DAL;
    using DBSoft.EPM.DAL.CodeFirst.Models;
    using DBSoft.EPM.DAL.Enums;
    using DBSoft.EPM.DAL.Factories;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Queries;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.Market;
    using Microsoft.Framework.Configuration;
    using NSubstitute;
    using Web.Infrastructure;
    using Xunit;
    using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
    using EpmConfig = DBSoft.EPM.DAL.Mvc6.EpmConfig;

    public class ItemServiceTests : IClassFixture<ConfigFixture>
    {
        private readonly ItemService _service;
        private const int Armageddon = 643; // Untracked No Instance
        private const int Apocalypse = 640; // Tracked No Instance
        private const int Raven = 638; // Untracked Instance
        private const int Tempest = 638; // Tracked Instance

        public ItemServiceTests(ConfigFixture fixture)
        {
            MapperConfig.ConfigureMappings();
            const int ravenBlueprint = 688;
            const int battleship = 27;

            const int orkashu = 60008170;

            var config = new EpmConfig(fixture.GetConfiguration());
            var provider = new ConnectionStringProvider(config);
            var factory = new EpmEntitiesFactory(provider);

            var config1 = Substitute.For<IConfigurationService>();
            config1.GetSetting<int>(Arg.Any<string>(), ConfigurationType.FactoryLocation).Returns(orkashu);
            var users = Substitute.For<IUserService>();
            users.GetUserID(Arg.Any<string>()).Returns(1);

            var blueprints = Substitute.For<IBlueprintService>();
            blueprints.List().Returns(new List<BlueprintDTO> { new BlueprintDTO { BuildItemID = Raven, ItemID = ravenBlueprint } });

            var extensions = Substitute.For<IItemExtensionQueryFactory>();
            var e = Substitute.For<IItemExtensionQuery>();
            e.ToList().Returns(new List<ItemExtension>
            {
                new ItemExtension
                {
                    ItemID = Apocalypse,
                    Item = new Item
                    {
                        Name = "Apocalypse",
                        GroupID = battleship
                    },
                    MinimumStock = 8
                },
                new ItemExtension
                {
                    ItemID = Armageddon,
                    Item = new Item
                    {
                        Name = "Armageddon",
                        GroupID = battleship
                    }
                },
                new ItemExtension
                {
                    ItemID = Tempest,
                    Item = new Item
                    {
                        Name = "Tempest",
                        GroupID = battleship
                    }
                }
            });
            extensions.Create(Arg.Any<EPMContext>(), Arg.Any<int>()).Returns(e);

            var instances = Substitute.For<IBlueprintInstanceQueryFactory>();
            var i = Substitute.For<IBlueprintInstanceQuery>();
            i.SpecifyStation(Arg.Any<int>()).Returns(i);
            i.GetQuery().Returns(new List<BlueprintInstance>
            {
                new BlueprintInstance
                {
                    Blueprint = new Blueprint
                    {
                        BuildItemID = Raven,
                        BuildItem = new Item
                        {
                            ID = Raven,
                            Name = "Raven",
                            GroupID = battleship
                        }   
                    }
                },
                                new BlueprintInstance
                {
                    Blueprint = new Blueprint
                    {
                        BuildItemID = Tempest,
                        BuildItem = new Item
                        {
                            ID = Tempest,
                            Name = "Tempest",
                            GroupID = battleship
                        }
                    }
                }
            }.AsQueryable());
            instances.Create(Arg.Any<EPMContext>(), Arg.Any<int>()).Returns(i);

            _service = new ItemService(factory, config1, blueprints, users, extensions, instances);
        }

        // Test everything that returns a List<ItemDTO> or List<BuildableItemDTO>
        [Fact]
        public void List_ShouldIncludeVolumes()
        {
            var result = _service.List(new ItemRequest());
            Assert.IsTrue(result.Any(f => f.Volume != 0));
        }

        [Fact]
        public void ListProducible_ShouldIncludeVolumes()
        {
            var result = _service.ListProducible();
            Assert.IsTrue(result.Any(f => f.Volume != 0));
        }

        [Fact]
        public void ListProducibleMaterials_ShouldIncludeVolumes()
        {
            var result = _service.ListProducibleMaterials();
            Assert.IsTrue(result.First().Volume != 0);
        }

        [Fact]
        public void ListBuildable_ShouldIncludeVolumes()
        {
            var result = _service.ListBuildable("");
            Assert.IsTrue(result.All(f => f.Volume != 0));
        }

        [Fact]
        public void ListMaintainable_ShouldIncludeVolumes()
        {
            var result = _service.ListMaintainable("");
            Assert.IsTrue(result.First().Volume != 0);
        }

        [Fact]
        public void TrackedWithoutInstance_ShouldBeBuildable()
        {
            // Consumed BPC, BPO moved out of factory but not yet cleared
            var result = _service.ListBuildable("");
            Assert.IsNotNull(result.SingleOrDefault(f => f.ItemID == Apocalypse));
        }

        [Fact]
        public void UntrackedWithoutInstance_ShouldNotBeBuildable()
        {
            // Consumed BPC, BPO moved out of factory but not yet cleared
            var result = _service.ListBuildable("");
            Assert.IsNull(result.SingleOrDefault(f => f.ItemID == Armageddon));
        }

        [Fact]
        public void UntrackedInstancesAtFactory_ShouldBeBuildable()
        {
            var result = _service.ListBuildable("");
            Assert.IsNotNull(result.SingleOrDefault(f => f.ItemID == Raven));
        }

        [Fact]
        public void TrackedInstancesAtFactory_ShouldBeBuildable()
        {
            var result = _service.ListBuildable("");
            Assert.IsNotNull(result.SingleOrDefault(f => f.ItemID == Tempest));
        }
    }

    public class ConfigFixture
    {
        public IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder(".")
                .AddJsonFile("config.json");
            return builder.Build();
        }
    }
}
