﻿namespace Dbsoft.Epm.Test.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using DBSoft.EPM.DAL.DTOs;
    using DBSoft.EPM.DAL.Enums;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using DBSoft.EPM.DAL.Services.Materials;
    using NSubstitute;
    using Xunit;
    using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

    public class ProductionMaterialServiceTests
    {
        [Fact]
        public void WhenPosConfigured_List_ShouldFindMaterialsInPos()
        {
            const string token = "token";
            const int factory = 1;
            const int pos = 2;
            const int raven = 638;
            const int tritanium = 34;
            var assets = Substitute.For<IAssetService>();
            // If two calls are made to get assets then two trit will be found
            assets.ListByItem(Arg.Any<AssetServiceRequest>()).Returns(f => new List<AssetByItemDTO>
            {
                new AssetByItemDTO
                {
                    ItemID = tritanium,
                    Quantity = 1
                }
            });
            var queue = Substitute.For<IProductionQueueService>();
            queue.List(token).Returns(f => new List<ProductionQueueDto>
            {
                new ProductionQueueDto
                {
                    ItemId = raven,
                    Sold = 1
                }
            });
            var materials = Substitute.For<IBuildMaterialService>();
            materials.ListBuildable(token).Returns(f => new List<BuildMaterialDto>
            {
                new BuildMaterialDto
                {
                    ItemId = raven,
                    MaterialId = tritanium,
                    Quantity = 1
                }
            });
            var config = Substitute.For<IConfigurationService>();
            config.GetSetting<int>(token, ConfigurationType.FactoryLocation).Returns(factory);
            config.GetSetting<int>(token, ConfigurationType.PosLocation).Returns(pos);

            var service = new ProductionMaterialService(assets, queue, materials, config);
            var result = service.List(token);
            Assert.AreEqual(2, result.Sum(f => f.Inventory));
        }
    }
}
