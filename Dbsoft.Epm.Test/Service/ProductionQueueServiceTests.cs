﻿namespace Dbsoft.Epm.Test.Service
{
    using System;
    using System.Diagnostics;
    using Autofac;
    using DBSoft.EPM.DAL.Interfaces;
    using DBSoft.EPM.DAL.Requests;
    using DBSoft.EPM.DAL.Services;
    using Xunit;
    using System.Linq;
    using Infrastructure;
    using Web.Infrastructure;

    public class ProductionQueueServiceTests
    {
        private readonly IProductionQueueService _service;
        private readonly string _token;
        private readonly IContainer _container;
        private const int RadioXl = 17688;

        public ProductionQueueServiceTests()
        {
            MapperConfig.ConfigureMappings();
            _container = new IocContainerBuilder().Build();
            _service = _container.Resolve<IProductionQueueService>();
            var users = _container.Resolve<IUserService>();
            _token =  Guid.NewGuid().ToString();
            users.Authenticate(new SsoAuthenticateRequest
            {
                EveOnlineCharacter = "SJ Astralana",
                SessionToken = _token
            });
        }

        [Fact]
        public void WhenUntracked_InstanceShouldBeBuildable()
        {
            var items = _container.Resolve<IItemService>();
            items.UpdateItem(new UpdateItemRequest
            {
                ItemID = RadioXl,
                MinimumStock = 0,
                Token = _token
            });
            var queue = _service.List(_token).Where(f => f.MinimumStock == 0).ToList();
            Debug.Assert(queue.Any(f => f.ItemId == RadioXl));
        }
    }
}
