﻿namespace EPM.Webjob.Infrastructure
{
    using DbSoft.Cache.Aspect;
    using DBSoft.EPM.DAL.Interfaces;

    public static class CacheConfig
    {
        public static void Configure(IEpmConfig config)
        {
            CacheService.SessionProperty = "token";
            // CacheService.CacheProviderFactory = new RedisCacheProviderFactory(config.GetSetting("Cache:Hostname"), config.GetSetting("Cache:AccessKey"));
            CacheService.CacheProviderFactory = new CacheProviderFactory();
        }
    }
}
